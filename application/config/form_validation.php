<?php
$config=array('set_products_rules'=>array(

             array(
             	    'field'=>'pname',
             	    'label'=>'Product Name',
             	    'rules'=>'required'
                   ),
             array(
             	    'field'=>'price',
             	    'label'=>'Price',
             	    'rules'=>'required'
                   ),
             array(
             	    'field'=>'pdesc',
             	    'label'=>"Product's Description",
             	    'rules'=>"required"
                   ),
             array(
             	    'field'=>'ppt',
             	    'label'=>'Packing Type',
             	    'rules'=>'required'
                   ),
             array(
             	    'field'=>'ppf',
             	    'label'=>'Features',
             	    'rules'=>'required'
                   ),
             array(
             	    'field'=>'ppa',
             	    'label'=>'Application',
             	    'rules'=>'required'
                   ),
             array(
             	    'field'=>'pcategory',
             	    'label'=>'Category',
             	    'rules'=>'required'
                   )
              ),

     'set_admin_login_rules'=>array(
            
            array(
                      'field'=>'username',
                      'label'=>'Username',
                      'rules'=>'required|alpha'
                   ),
             array(
                      'field'=>'password',
                      'label'=>'Password',
                      'rules'=>'required|max_length[12]'
                   )
                       ),
     'set_users_reg_rules'=>array(

             array(
                  'field'=>'fname',
                  'label'=>'First Name',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'lname',
                  'label'=>'Last Name',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'username',
                  'label'=>"Username",
                  'rules'=>"required"
                   ),
             array(
                  'field'=>'password',
                  'label'=>'Password',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'email',
                  'label'=>'Email',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'address',
                  'label'=>'Address',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'city',
                  'label'=>'City',
                  'rules'=>'required'
                   ),

             array(
                  'field'=>'country',
                  'label'=>'Country',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'postal_code',
                  'label'=>'Postal Code',
                  'rules'=>'required'
                   ),
             array(
                  'field'=>'status',
                  'label'=>'status',
                  'rules'=>'required'
                   )
              ),
      'set_users_login_rules'=>array(
            
            array(
                      'field'=>'username',
                      'lable'=>'Username',
                      'rules'=>'required|alpha'
                   ),
             array(
                      'field'=>'password',
                      'lable'=>'Password',
                      'rules'=>'required|max_length[12]'
                   )
                       ),
      'set_notification_rules'=>array(
            
            array(
                      'field'=>'title',
                      'label'=>'Notification title',
                      'rules'=>'required'
                   )
                       )


              );
?>