-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2019 at 02:58 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `parag`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`id`, `username`, `password`) VALUES
(1, 'admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `rowid` varchar(123) NOT NULL,
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `packing_type` varchar(255) NOT NULL,
  `qty` int(5) NOT NULL,
  `total_price` float(12,2) NOT NULL,
  `user` varchar(255) NOT NULL,
  `added_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`rowid`, `id`, `product_name`, `price`, `packing_type`, `qty`, `total_price`, `user`, `added_at`) VALUES
('85bf60667947b0', 8, 'PARAG Gold (F.C.M.)', 87654.00, 'Poly Pack - 500ml, 1000ml', 30, 87654.00, 'user', '2018-11-22 01:29:15'),
('85bf6df37d8c9a', 8, 'PARAG Gold (F.C.M.)', 2345.00, 'Poly Pack - 500ml, 1000ml', 203, 2345.00, 'user', '2018-11-22 16:54:20'),
('85bf6dffe3e434', 8, 'X-pect', 80.00, 'packing_type', 3, 80.00, 'user', '2018-11-22 16:57:41'),
('9', 7, 'mashood', 10.00, 'bra penty', 112, 1120.00, 'user', '2018-11-20 21:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `momin_the_great`
--

CREATE TABLE `momin_the_great` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `path` varchar(255) NOT NULL,
  `sent_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `momin_the_great`
--

INSERT INTO `momin_the_great` (`id`, `title`, `path`, `sent_at`) VALUES
(9, 'dfsdj', 'http://localhost/parag/assets/images/uploads/', '2018-11-21 01:31:59'),
(10, 'Momin', 'http://localhost/parag/assets/images/uploads/', '2018-11-21 01:40:52'),
(11, 'Momin', 'http://localhost/parag/assets/images/uploads/', '2018-11-21 01:41:23');

-- --------------------------------------------------------

--
-- Table structure for table `my_cart`
--

CREATE TABLE `my_cart` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float(7,2) NOT NULL,
  `qty` int(6) NOT NULL,
  `packing_type` varchar(250) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `price` float(10,2) NOT NULL,
  `description` text NOT NULL,
  `packing_type` varchar(255) NOT NULL,
  `product_features` text NOT NULL,
  `product_application` text NOT NULL,
  `category` varchar(255) NOT NULL,
  `product_image_path` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `price`, `description`, `packing_type`, `product_features`, `product_application`, `category`, `product_image_path`, `status`) VALUES
(21, 'PARAG Gold (F.C.M.)', 2345.00, 'Pasteurised milk Parag milk meets the PFA standards for the respective type of milk.', 'Poly Pack - 500ml, 1000ml', 'Parag Milk is the most hygenic liquid milk available in the market.It is pasteurised in state-of-the-art processing plants and pouch-packed to make it conveniently available to consumers.', 'Direct consumption, Making of : Tea or Coffee, Sweets, Khoa, Curd, Buttermilk, Ghee', 'Milk', 'http://localhost/parag/assets/images/favicon2.png', 1),
(22, 'X-pect', 80.00, 'Dextromethorphan hydrobromide', 'packing_type', 'x-pect', 'app', 'Milk', 'http://localhost/parag/assets/images/favicon3.png', 1),
(23, 'Shivam', 434.00, 'Des', 'Poly Pack - 500ml, 1000ml', 'demo', 'demo', 'Milk', 'http://localhost/parag/assets/images/', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_cart`
--

CREATE TABLE `product_cart` (
  `id` varchar(255) NOT NULL,
  `qty` int(6) DEFAULT '1',
  `price` float(7,2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_cart`
--

INSERT INTO `product_cart` (`id`, `qty`, `price`, `name`, `username`) VALUES
('101755707', 1, 543.00, 'Milk', 'user'),
('1017804575', 1, 543.00, 'Milk', 'user'),
('1036548399', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1073840508', 1, 10.00, 'mashood', 'user'),
('109268015', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1093948033', 1, 543.00, 'Milk', 'user'),
('109659691', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1100897599', 1, 10.00, 'mashood', 'user'),
('1166552506', 1, 10.00, 'mashood', ''),
('1166943586', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('117008934', 1, 10.00, 'mashood', 'user'),
('1181199728', 1, 543.00, 'Milk', 'user'),
('1212807817', 1, 10.00, 'mashood', 'user'),
('1220177832', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1233364597', 1, 10.00, 'mashood', 'user'),
('1235835875', 1, 10.00, 'mashood', 'user'),
('1317105753', 1, 10.00, 'mashood', 'user'),
('1331065432', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('133268284', 1, 543.00, 'Milk', 'user'),
('1333764212', 1, 10.00, 'mashood', 'user'),
('1333951426', 1, 10.00, 'mashood', 'user'),
('1343197833', 1, 10.00, 'mashood', 'user'),
('1389171047', 1, 10.00, 'mashood', 'user'),
('1405555258', 1, 10.00, 'mashood', 'user'),
('1439870769', 1, 10.00, 'mashood', 'user'),
('1457485508', 1, 10.00, 'mashood', 'user'),
('1464586712', 1, 10.00, 'mashood', 'user'),
('1485121421', 1, 10.00, 'mashood', 'user'),
('1501002480', 1, 10.00, 'mashood', 'user'),
('151015959', 1, 543.00, 'Milk', 'user'),
('1524829578', 1, 10.00, 'mashood', 'user'),
('1533070670', 1, 543.00, 'Milk', 'user'),
('1537461194', 1, 543.00, 'Milk', 'user'),
('15560945', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1568011284', 1, 10.00, 'mashood', 'user'),
('15975989', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1681311577', 1, 10.00, 'mashood', 'user'),
('1693058843', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1709220884', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('1710448645', 1, 10.00, 'mashood', 'user'),
('1722202552', 1, 10.00, 'mashood', 'user'),
('1722278323', 1, 10.00, 'mashood', 'user'),
('1745507997', 1, 10.00, 'mashood', 'user'),
('1779003115', 1, 10.00, 'mashood', 'user'),
('1797283535', 1, 10.00, 'mashood', 'user'),
('1808815680', 1, 10.00, 'mashood', 'user'),
('1821508850', 1, 10.00, 'mashood', 'user'),
('1870688063', 1, 543.00, 'Milk', 'user'),
('19093902', 1, 10.00, 'mashood', 'user'),
('1928306296', 1, 10.00, 'mashood', 'user'),
('1940662379', 1, 10.00, 'mashood', 'user'),
('1966534554', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('197903565', 1, 10.00, 'mashood', 'user'),
('2014672516', 1, 543.00, 'Milk', 'user'),
('2016346463', 1, 10.00, 'mashood', 'user'),
('2024414831', 1, 10.00, 'mashood', 'user'),
('2090253751', 1, 543.00, 'Milk', 'user'),
('20922408', 1, 10.00, 'mashood', 'user'),
('2094642288', 1, 10.00, 'mashood', 'user'),
('2121549134', 1, 10.00, 'mashood', 'user'),
('2124572717', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('2127139004', 1, 10.00, 'mashood', 'user'),
('2139078826', 1, 10.00, 'mashood', 'user'),
('2140050449', 1, 10.00, 'mashood', 'user'),
('2143900314', 1, 10.00, 'mashood', 'user'),
('2144562513', 1, 10.00, 'mashood', 'user'),
('245066433', 1, 10.00, 'mashood', 'user'),
('264489240', 1, 10.00, 'mashood', 'user'),
('27722120', 1, 10.00, 'mashood', 'user'),
('285397931', 1, 10.00, 'mashood', 'user'),
('286156533', 1, 10.00, 'mashood', 'user'),
('309568597', 1, 10.00, 'mashood', 'user'),
('322337715', 1, 10.00, 'mashood', 'user'),
('324649525', 1, 10.00, 'mashood', 'user'),
('351031114', 1, 10.00, 'mashood', 'user'),
('351036218', 1, 543.00, 'Milk', 'user'),
('365470268', 1, 10.00, 'mashood', 'user'),
('376657355', 1, 543.00, 'Milk', 'user'),
('380934122', 1, 10.00, 'mashood', 'user'),
('386415700', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('3928091', 1, 10.00, 'mashood', 'user'),
('442828898', 1, 10.00, 'mashood', 'user'),
('448297882', 1, 10.00, 'mashood', 'user'),
('480439256', 1, 543.00, 'Milk', 'user'),
('549375410', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('557024301', 1, 543.00, 'Milk', 'user'),
('581756924', 1, 10.00, 'mashood', 'user'),
('610080132', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('638991904', 1, 543.00, 'Milk', 'user'),
('640494577', 1, 10.00, 'mashood', 'user'),
('65184431', 1, 10.00, 'mashood', 'user'),
('668357740', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('67013055', 1, 543.00, 'Milk', 'user'),
('678568305', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('68425547', 1, 543.00, 'Milk', 'user'),
('693442712', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('703735618', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('705817741', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('709865', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('710921181', 1, 10.00, 'mashood', 'user'),
('717851549', 1, 10.00, 'mashood', 'user'),
('720037220', 1, 10.00, 'mashood', 'user'),
('732922259', 1, 543.00, 'Milk', 'user'),
('7739266', 1, 10.00, 'mashood', 'user'),
('823910149', 1, 10.00, 'mashood', 'user'),
('847798499', 1, 10.00, 'mashood', 'user'),
('87021033', 1, 10.00, 'mashood', 'user'),
('892681061', 1, 10.00, 'mashood', 'user'),
('937693823', 1, 456.00, 'PARAG Gold (F.C.M.)', 'user'),
('969703049', 1, 10.00, 'mashood', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `product_category`
--

CREATE TABLE `product_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id`, `category_name`) VALUES
(1, 'Milk'),
(2, 'Ghee');

-- --------------------------------------------------------

--
-- Table structure for table `user_reg`
--

CREATE TABLE `user_reg` (
  `id` int(11) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(110) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(100) NOT NULL,
  `postal_code` int(6) NOT NULL,
  `country` varchar(100) NOT NULL,
  `whoru` varchar(255) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_reg`
--

INSERT INTO `user_reg` (`id`, `fname`, `lname`, `username`, `password`, `email`, `address`, `city`, `postal_code`, `country`, `whoru`, `status`) VALUES
(1, 'Ram', 'Gopal', 'User', '123', 'User@gmail.com', 'Kamla nehru', 'Sultanpur', 228118, 'India', 'distributor', 1),
(2, 'momin', 'raza', 'itsme', '123456', 'some@example.com', 'Shishgarh', 'Bareilly', 987654, 'India', 'distributor', 1),
(3, 'Vidya', 'Sagar', 'vidyasagar', '123', 'vidya@gmail.com', 'basti', 'basti', 228118, 'India', 'agent', 1),
(4, 'Mashood', 'Alam', 'mas', '123', 'mashood@gmail.com', 'prayagraj', 'Alahabad', 123456, 'India', 'agent', 2),
(5, 'Momin', 'Raza', 'momin_raza', 'mominRaza786', 'someExampl@gmail.com', 'address', 'city', 123456, 'india', 'distributor', 1),
(7, 'firstName', 'lastname', 'username', 'password', 'email@email.com', 'address', 'city', 456789, 'india', 'agent', 1),
(8, '123', '43', 'gfd', 'bvcvc', 'c', 'vc', 'f', 654321, 'gf', 'agent', 1),
(12, 'sdfg', 'fghjk', 'sdxvxv', '1234', 'sdf', 'dfd', 'df', 1234566, 'sdsf', 'distributor', 2),
(13, 'Someexampe', 'example', 'wqert', '2345', 'someasadf@example.com', 'address', 'city', 123456, 'sdfsgd', 'Distruibutor', 0),
(14, 'hfdeo', 'fdlsdjf', 'fsldkj', 'pqerf', 'em@f.co', 'address', 'city', 234567, 'India', 'distributer', 0),
(15, 'Progress', 'surename', 'Progress_hello', '123456', 'dfs@gmail.com', 'fds', 'city', 123456, 'country', 'distributer', 1),
(16, '', '', '', '', '', '', '', 0, '', '', 1),
(17, 'rudra', 'pratap singh', 'rudra2351`', '123456', 'rudrapratap7518@gmail.com', 'knit', 'sultanpur', 221007, 'india', 'distributer', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`rowid`);

--
-- Indexes for table `momin_the_great`
--
ALTER TABLE `momin_the_great`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_cart`
--
ALTER TABLE `my_cart`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_cart`
--
ALTER TABLE `product_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_category`
--
ALTER TABLE `product_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_reg`
--
ALTER TABLE `user_reg`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `momin_the_great`
--
ALTER TABLE `momin_the_great`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `my_cart`
--
ALTER TABLE `my_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `product_category`
--
ALTER TABLE `product_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_reg`
--
ALTER TABLE `user_reg`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
